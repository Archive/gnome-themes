# Brazilian Portuguese translation of gnome-themes.
# Copyright (C) 2004-2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-themes package.
# Alexandre Folle de Menezes <afmenez@terra.com.br>, 2003,2005.
# Evandro Fernandes Giovanini <evandrofg@ig.com.br>, 2004.
# Og Maciel <ogmaciel@ubuntu.com>, 2006-2007.
# Djavan Fagundes <dnoway@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-themes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-11-07 13:33+0100\n"
"PO-Revision-Date: 2009-01-09 10:20-0300\n"
"Last-Translator: Djavan Fagundes <dnoway@gmail.com>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../desktop-themes/Clearlooks/index.theme.in.h:1
msgid "Attractive Usability"
msgstr "Usabilidade atrativa"

#: ../desktop-themes/Clearlooks/index.theme.in.h:2
msgid "Clearlooks"
msgstr "Clearlooks"

#: ../desktop-themes/ClearlooksTest/index.theme.in.h:1
msgid "Clearlooks Test Theme"
msgstr "Tema de teste Clearlooks"

#: ../desktop-themes/ClearlooksTest/index.theme.in.h:2
msgid "Theme with a different colorscheme to find problems in applications."
msgstr ""
"Tema com um esquema de cor diferente, para encontrar problemas em "
"aplicativos."

#: ../desktop-themes/Crux/index.theme.in.h:1
#: ../icon-themes/Crux/index.theme.in.h:1
msgid "Crux"
msgstr "Crux"

#: ../desktop-themes/Crux/index.theme.in.h:2
msgid "Smooth gradients with purple highlights"
msgstr "Gradientes suaves com destaques em roxo"

#: ../desktop-themes/Glider/index.theme.in.h:1
msgid "Glider"
msgstr "Glider"

#: ../desktop-themes/Glider/index.theme.in.h:2
msgid "Simple, smooth and aesthetically pleasing"
msgstr "Simples, suave e esteticamente agradável"

#: ../desktop-themes/Glossy/index.theme.in.h:1
msgid "A glossy looking theme"
msgstr "Um tema de aparência brilhante"

#: ../desktop-themes/Glossy/index.theme.in.h:2
msgid "Glossy"
msgstr "Glossy"

#: ../desktop-themes/HighContrast/index.theme.in.h:1
msgid "Black-on-white text and icons"
msgstr "Texto e ícones preto-no-branco"

#: ../desktop-themes/HighContrast/index.theme.in.h:2
msgid "High Contrast"
msgstr "Alto contraste"

#: ../desktop-themes/HighContrastInverse/index.theme.in.h:1
msgid "High Contrast Inverse"
msgstr "Alto contraste inverso"

#: ../desktop-themes/HighContrastInverse/index.theme.in.h:2
msgid "White-on-black text and icons"
msgstr "Texto e ícones branco-no-preto"

#: ../desktop-themes/HighContrastLargePrint/index.theme.in.in.h:1
#: ../icon-themes/HighContrastLargePrint/index.theme.in.h:1
msgid "High Contrast Large Print"
msgstr "Alto contraste letras grandes"

#: ../desktop-themes/HighContrastLargePrint/index.theme.in.in.h:2
msgid "Large black-on-white text and icons"
msgstr "Texto e ícones preto-no-branco grandes"

#: ../desktop-themes/HighContrastLargePrintInverse/index.theme.in.in.h:1
#: ../icon-themes/HighContrastLargePrintInverse/index.theme.in.h:1
msgid "High Contrast Large Print Inverse"
msgstr "Alto contraste letras grandes inverso"

#: ../desktop-themes/HighContrastLargePrintInverse/index.theme.in.in.h:2
msgid "Large white-on-black text and icons"
msgstr "Texto e ícones branco-no-preto grandes"

#: ../desktop-themes/LargePrint/index.theme.in.in.h:1
msgid "Large Print"
msgstr "Letras grandes"

#: ../desktop-themes/LargePrint/index.theme.in.in.h:2
msgid "Large text and icons"
msgstr "Texto e ícones grandes"

#: ../desktop-themes/LowContrast/index.theme.in.h:1
msgid "Low Contrast"
msgstr "Baixo contraste"

#: ../desktop-themes/LowContrast/index.theme.in.h:2
msgid "Muted text and icons"
msgstr "Texto e ícones emudecidos"

#: ../desktop-themes/LowContrastLargePrint/index.theme.in.in.h:1
msgid "Large muted text and icons"
msgstr "Texto e ícones emudecidos grandes"

#: ../desktop-themes/LowContrastLargePrint/index.theme.in.in.h:2
msgid "Low Contrast Large Print"
msgstr "Letras grandes de baixo contraste"

#: ../desktop-themes/Mist/index.theme.in.h:1
msgid "A minimalistic appearance"
msgstr "Uma aparência minimalista"

#: ../desktop-themes/Mist/index.theme.in.h:2
#: ../icon-themes/Mist/index.theme.in.h:1
msgid "Mist"
msgstr "Neblina"

#: ../icon-themes/HighContrast-SVG/index.theme.in.h:1
msgid "High contrast scalable icons with black borders"
msgstr "Ícones escaláveis de alto contraste com bordas negras"

#: ../icon-themes/HighContrast-SVG/index.theme.in.h:2
msgid "HighContrast-SVG"
msgstr "SVG de alto contraste"

#: ../icon-themes/HighContrastLargePrint/index.theme.in.h:2
msgid "Large High Contrast Icon Theme"
msgstr "Tema de ícone grande de alto contraste"

#: ../icon-themes/HighContrastLargePrintInverse/index.theme.in.h:2
msgid "Large Inverse High Contrast Icon Theme"
msgstr "Tema de ícone grande de alto contraste inverso"

#~ msgid "Dusty orange appearance"
#~ msgstr "Aparência laranja empoeirada"

#~ msgid "Grand Canyon"
#~ msgstr "Grand Canyon"

#~ msgid "Ocean Dream"
#~ msgstr "Ocean Dream"

#~ msgid "The colours of the seashore"
#~ msgstr "As cores do litoral"

#~ msgid "Simple"
#~ msgstr "Simple"

#~ msgid "Thin appearance, high performance"
#~ msgstr "Aparência leve, alta performance"

#~ msgid "A soft glassy appearance"
#~ msgstr "Aparência levemente cristalina"

#~ msgid "Smokey Blue"
#~ msgstr "Azul Esfumaçado"

#~ msgid "Traditional"
#~ msgstr "Tradicional"

#~ msgid "Traditional hard-edged 3D appearance"
#~ msgstr "Aparência tradicional de bordas fortes em 3D"

#~ msgid "Flat-Blue"
#~ msgstr "Flat-Blue"

#~ msgid "Sandy"
#~ msgstr "Arenoso"

#~ msgid "Smokey-Blue"
#~ msgstr "Azul-Esfumaçado"

#~ msgid "Smokey-Red"
#~ msgstr "Vermelho-Esfumaçado"
